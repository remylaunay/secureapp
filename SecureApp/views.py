from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404
from django_pivot.pivot import pivot
from rolepermissions.roles import assign_role
from rolepermissions.checkers import has_permission
from SecureApp.forms import UserForm, MessageForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from SecureApp.models import Message


def message_list(request):
    listmessages = Message.objects.all()
    paginator = Paginator(listmessages, 5)  # Show 25 contacts per page
    page = request.GET.get('page')
    messages = paginator.get_page(page)
    return render(request, 'messages/list.html', {'messages': messages})


def login_failed(request):
    return render(request, 'users/login_failed.html', {})


@login_required(login_url="/user/login")
def message_creation(request):
    if request.method == 'POST':
        message_form = MessageForm(data=request.POST)
        if message_form.is_valid():
            message = Message(author_id=request.user.id, content=request.POST.get('content'))
            message.save()
            return HttpResponseRedirect(reverse('message_list'))
        return render(request, 'messages/create.html')
    return render(request, 'messages/create.html')


@login_required(login_url="/user/login")
def message_deletion(request, pk):
    if request.method == 'GET':
        message = get_object_or_404(Message, pk=pk)
        if has_permission(request.user, 'remove_all_messages') or (has_permission(request.user, 'remove_own_messages')
                                                                   and message.author_id == request.user.id):
            message_id = message.id
            message.delete()
            return render(request, 'messages/deleted.html', {'message_id': message_id})
        return HttpResponseRedirect(reverse('elevation'))
    return HttpResponseRedirect(reverse('error'))


def message_deleted(request):
    return HttpResponseRedirect(reverse('message/deleted'))


@login_required(login_url="/user/login")
def special(request):
    return HttpResponseRedirect(reverse('message_list'))


@login_required(login_url="/user/login")
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('message_list'))


def user_register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            assign_role(user, 'chatter')
            registered = True
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()
    return render(request, 'users/register.html',
                  {'user_form': user_form,
                   'registered': registered})


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('message_list'))
            else:
                return HttpResponse("Votre compte est inactif.")
        else:
            print("Authentification échouée {} : {}".format(username, password))
            return HttpResponseRedirect(reverse('login_failed'))
    else:
        return render(request, 'users/login.html', {})


def user_list(request):
    user_list = User.objects.all()
    paginator = Paginator(user_list, 5)  # Show 25 contacts per page
    page = request.GET.get('page')
    users = paginator.get_page(page)
    return render(request, 'users/list.html', {'users': users})


def user_edit(request, pk):
    user = User.objects.get(pk)

    return render(request, 'users/edit.html', {'user': user})


def elevation(request):
    return render(request, 'users/elevation.html')


def error(request):
    return render(request, 'users/error.html')
