from django.contrib import admin
from .models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'author', 'content')
    list_filter = ("created_on",)
    search_fields = ['author', 'content']


admin.site.register(Message, MessageAdmin)