from rolepermissions.roles import AbstractUserRole

class Admin(AbstractUserRole):
    available_permissions = {
        'edit_all_messages': True,
        'edit_own_messages': True,
        'remove_all_messages': True,
        'remove_own_messages': True,
    }

class Chatter(AbstractUserRole):
    available_permissions = {
        'edit_own_messages': True,
        'remove_own_messages': True,
    }
