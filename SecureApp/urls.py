# SecureApp/urls.py
from django.conf.urls import url
from SecureApp import views

# SET THE NAMESPACE!
app_name = 'SecureApp'
# Be careful setting the name to just /login use userlogin instead!
urlpatterns = [
    url(r'^$', views.message_list, name='message_list'),
    url(r'^user/register/$', views.user_register, name='user_register'),
    url(r'^user/login/$', views.user_login, name='user_login'),
    url(r'^messages/create/$', views.message_creation, name='message_creation'),
    url(r'^messages/delete/(?P<pk>.*)', views.message_deletion, name='message_deletion'),
    url(r'^user/logout/$', views.user_logout, name='user_logout'),
    url(r'^elevation/$', views.elevation, name='elevation'),
    url(r'^messages/deleted/$', views.message_deleted, name='message_deleted'),
    url(r'^user/login_failed/$', views.login_failed, name='login_failed'),
    url(r'^error/$', views.error, name='error'),
    url(r'^user/list/$', views.user_list, name='user_list'),
]
