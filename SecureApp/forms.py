from django import forms
from django.core.validators import validate_email
from django.contrib.auth.models import User
from SecureApp.models import Message


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label="Mot de passe", min_length=8)
    username = forms.CharField(label="Nom d'utilisateur", min_length=3)
    email = forms.EmailField(label="Email", validators=[validate_email])

    class Meta:
        model = User
        fields = ('username', 'password', 'email')


class MessageForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ('content',)
